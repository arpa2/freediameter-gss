#! /bin/sh
#
# Usage:
#
#   kerberos.sh <user> <password>
#
# Does no error-checking at all.

echo "$2" | kinit $1
echo "--"
