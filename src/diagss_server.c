/* diagss_serv.c -- freeDiameter Extension app/server for Diameter-GSS
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Sebastien Decugis <sdecugis@freediameter.net>
 */

/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2013, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

/* Install the dispatch callbacks */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <unistd.h>

#include <arpa2/socket.h>

#include <arpa2/quick-der.h>

#include <arpa2/diagsslib.h>

#include <gssapi/gssapi_generic.h>
#include <gssapi/gssapi_krb5.h>
#include <gssapi/gssapi_ext.h>

#include "diagss_server_app.h"


static struct disp_hdl * diagss_hdl_fb = NULL; /* handler for fallback cb */
static struct disp_hdl * diagss_hdl_tr = NULL; /* handler for Test-Request req cb */

static struct session_handler * sess_handler = NULL;

struct sess_state {
};

static gss_cred_id_t server_creds;
static gss_ctx_id_t context;

static int
server_acquire_creds(gss_buffer_desc name_buf, gss_OID mech,
                     gss_cred_id_t *server_creds);

static void
display_status_1(m, code, type)
    char   *m;
    OM_uint32 code;
    int     type;
{
    OM_uint32 min_stat;
    gss_buffer_desc msg;
    OM_uint32 msg_ctx;

    msg_ctx = 0;
    while (1) {
        (void) gss_display_status(&min_stat, code, type, GSS_C_NULL_OID,
                                  &msg_ctx, &msg);
        fd_log_error("GSS-API error %s: %s", m, (char *) msg.value);
        (void) gss_release_buffer(&min_stat, &msg);

        if (!msg_ctx)
            break;
    }
}

/*
 * Function: display_status
 *
 * Purpose: displays GSS-API messages
 *
 * Arguments:
 *
 *      msg             a string to be displayed with the message
 *      maj_stat        the GSS-API major status code
 *      min_stat        the GSS-API minor status code
 *
 * Effects:
 *
 * The GSS-API messages associated with maj_stat and min_stat are
 * displayed on stderr, each preceded by "GSS-API error <msg>: " and
 * followed by a newline.
 */
void
display_status(msg, maj_stat, min_stat)
    char   *msg;
    OM_uint32 maj_stat;
    OM_uint32 min_stat;
{
    display_status_1(msg, maj_stat, GSS_C_GSS_CODE);
    display_status_1(msg, min_stat, GSS_C_MECH_CODE);
}


/* Default callback for the application. */
static int diagss_fb_cb( struct msg ** msg, struct avp * avp, struct session * sess, void * opaque, enum disp_action * act)
{
	/* This CB should never be called */
	TRACE_ENTRY("%p %p %p %p", msg, avp, sess, act);
	
	fd_log_debug("Unexpected message received!");
	
	return ENOTSUP;
}

/* Callback for incoming Test-Request messages */
static int diagss_tr_cb( struct msg ** msg, struct avp * avp, struct session * sess, void * opaque, enum disp_action * act)
{
	struct msg *ans, *qry;
	struct avp *gss_token;
	struct avp_hdr *dest_realm_hdr = NULL;
	
	fd_log_debug("ENTRY: %p %p %p %p", msg, avp, sess, act);
	
	if (msg == NULL)
		return EINVAL;
	

	/* Value of Origin-Host */
	struct avp *origin_host;
	CHECK_FCT( fd_msg_search_avp ( *msg, diagss_origin_host, &origin_host) );
	if (origin_host) {
		struct avp_hdr * hdr;
		CHECK_FCT( fd_msg_avp_hdr( origin_host, &hdr ) );
		fd_log_debug("Origin-Host: %.*s", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
	} else {
		fd_log_debug("no_Origin-Host");
	}
	/* Value of Destination-Realm */
	struct avp *dest_realm;
	CHECK_FCT( fd_msg_search_avp ( *msg, diagss_dest_realm, &dest_realm) );
	if (dest_realm) {
		CHECK_FCT( fd_msg_avp_hdr( dest_realm, &dest_realm_hdr ) );
		fd_log_debug("Destination-Realm: %.*s", (int)dest_realm_hdr->avp_value->os.len, dest_realm_hdr->avp_value->os.data);
	} else {
		fd_log_debug("no Destination-Realm");
	}
	fd_log_debug("replying...");
	
	/* Create answer header */
	qry = *msg;
	CHECK_FCT( fd_msg_new_answer_from_req ( fd_g_config->cnf_dict, msg, 0 ) );
	ans = *msg;
	
	char *rescode = "DIAMETER_UNABLE_TO_COMPLY";
	CHECK_FCT_DO( fd_msg_search_avp ( qry, diagss_avp_gss_token, &gss_token), goto out );

	if (gss_token != NULL) {
		gss_OID mech = GSS_C_NO_OID;

		char *realm;
		int realm_len;
		char *service;
		int service_len;
		char *host;
		int host_len;


		struct avp_hdr * hdr = NULL;
		CHECK_FCT( fd_msg_avp_hdr( gss_token, &hdr )  );
		fd_log_debug("Found GSS-Token AVP");
		//fd_log_debug("Destination-Realm: %.*s", (int)dest_realm_hdr->avp_value->os.len, dest_realm_hdr->avp_value->os.data);
		gss_buffer_desc name_buf;
		int r = get_spn_from_spnego(
			hdr->avp_value->os.data, hdr->avp_value->os.len,
			&realm, &realm_len,
			&service, &service_len,
			&host, &host_len
		);
		
		if (r == 0) {
			fd_log_debug("realm: %.*s", realm_len, realm);
			fd_log_debug("service: %.*s", service_len, service);
			fd_log_debug("host: %.*s", host_len, host);
			name_buf.value = service;
			name_buf.length = service_len;
			if (server_acquire_creds(name_buf, mech, &server_creds) < 0)
				return -1;
		}

		gss_buffer_desc send_tok, recv_tok;
		gss_name_t client;
		gss_OID doid;
		OM_uint32 maj_stat, min_stat, acc_sec_min_stat;
		gss_buffer_desc oid_name;
		OM_uint32 ret_flags;
		recv_tok.value = hdr->avp_value->os.data;
		recv_tok.length = hdr->avp_value->os.len;

		maj_stat = gss_accept_sec_context(&acc_sec_min_stat, &context,
										  server_creds, &recv_tok,
										  GSS_C_NO_CHANNEL_BINDINGS,
										  &client, &doid, &send_tok,
										  &ret_flags,
										  NULL,  /* time_rec */
										  NULL); /* del_cred_handle */

		if (send_tok.length != 0) {
			fd_log_debug("Sending accept_sec_context token (size=%d):", send_tok.length);

			union avp_value result = {
				.os = {
					.data = send_tok.value,
					.len = send_tok.length
				}
			};
			CHECK_FCT( fd_msg_avp_new ( diagss_avp_gss_token, 0, &avp ) );
			CHECK_FCT( fd_msg_avp_setvalue( avp, &result ) );
			CHECK_FCT( fd_msg_avp_add( ans, MSG_BRW_LAST_CHILD, avp ) );
			(void) gss_release_buffer(&min_stat, &send_tok);
			switch (maj_stat) {
			case GSS_S_COMPLETE: {
				gss_buffer_desc client_name;

				rescode = "DIAMETER_SUCCESS";
				maj_stat = gss_display_name(&min_stat, client, &client_name, &doid);
				if (maj_stat != GSS_S_COMPLETE) {
					display_status("displaying name", maj_stat, min_stat);
					goto out;
				}
				fd_log_debug(
					"Accepted connection: \"%.*s\"\n",
					(int) client_name.length,
					(char *) client_name.value
				);
				union avp_value user_name = {
					.os = {
						.data = client_name.value,
						.len  = client_name.length
					}
				};
				CHECK_FCT( fd_msg_avp_new ( diagss_user_name, 0, &avp ) );
				CHECK_FCT( fd_msg_avp_setvalue( avp, &user_name ) );
				CHECK_FCT( fd_msg_avp_add( ans, MSG_BRW_LAST_CHILD, avp ) );
				(void) gss_release_buffer(&min_stat, &client_name);
				maj_stat = gss_release_name(&min_stat, &client);
				if (maj_stat != GSS_S_COMPLETE) {
					display_status("releasing name", maj_stat, min_stat);
					return -1;
				}
				context = GSS_C_NO_CONTEXT;
				break;
			}
			case GSS_S_CONTINUE_NEEDED:
				rescode = "DIAMETER_MULTI_ROUND_AUTH";
				break;
			default:
				display_status("gss_accept_sec_context", maj_stat, acc_sec_min_stat);
				break;
			}
		}
	} else {
		fd_log_debug("GSS-Token AVP *not* found");
	}


diagssout:
	fd_log_debug("Result Code: %s", rescode);
	
	/* Set the Origin-Host, Origin-Realm, Result-Code AVPs */
	CHECK_FCT( fd_msg_rescode_set( ans, rescode, NULL, NULL, 1 ) );
	
	/* Send the answer */
	CHECK_FCT( fd_msg_send( msg, NULL, NULL ) );

	return 0;
out:
	return -1;
}

/*
 * Function: server_acquire_creds
 *
 * Purpose: imports a service name and acquires credentials for it
 *
 * Arguments:
 *
 *      name_buf        (r) the ASCII service name in a gss_buffer_desc
 *      mech            (r) the desired mechanism (or GSS_C_NO_OID)
 *      server_creds    (w) the GSS-API service credentials
 *
 * Returns: 0 on success, -1 on failure
 *
 * Effects:
 *
 * The service name is imported with gss_import_name, and service
 * credentials are acquired with gss_acquire_cred.  If either operation
 * fails, an error message is displayed and -1 is returned; otherwise,
 * 0 is returned.  If mech is given, credentials are acquired for the
 * specified mechanism.
 */

static int
server_acquire_creds(gss_buffer_desc name_buf, gss_OID mech,
                     gss_cred_id_t *server_creds)
{
    gss_name_t server_name;
    OM_uint32 maj_stat, min_stat;
    gss_OID_set_desc mechlist;
    gss_OID_set mechs = GSS_C_NO_OID_SET;

    maj_stat = gss_import_name(&min_stat, &name_buf,
                               (gss_OID) gss_nt_service_name, &server_name);
    if (maj_stat != GSS_S_COMPLETE) {
        display_status("importing name", maj_stat, min_stat);
        return -1;
    }

    if (mech != GSS_C_NO_OID) {
        mechlist.count = 1;
        mechlist.elements = mech;
        mechs = &mechlist;
    }
    maj_stat = gss_acquire_cred(&min_stat, server_name, 0, mechs, GSS_C_ACCEPT,
                                server_creds, NULL, NULL);
    if (maj_stat != GSS_S_COMPLETE) {
        display_status("acquiring credentials", maj_stat, min_stat);
        return -1;
    }

    (void) gss_release_name(&min_stat, &server_name);

    return 0;
}


int diagss_serv_init(void)
{
	struct disp_when data;
	
	context = GSS_C_NO_CONTEXT;
	if (krb5_gss_register_acceptor_identity(diagss_conf->keytab)) {
        fd_log_error("failed to register keytab");
		return -1;
    }

	CHECK_FCT( fd_sess_handler_create(&sess_handler, (void *)free, NULL, NULL) );
	
	TRACE_DEBUG(FULL, "Initializing dispatch callbacks for test");
	
	memset(&data, 0, sizeof(data));
	data.app = diagss_appli;
	data.command = diagss_cmd_r;

	/* fallback CB if command != Test-Request received */
	CHECK_FCT( fd_disp_register( diagss_fb_cb, DISP_HOW_APPID, &data, NULL, &diagss_hdl_fb ) );
	
	/* Now specific handler for Test-Request */
	CHECK_FCT( fd_disp_register( diagss_tr_cb, DISP_HOW_CC, &data, NULL, &diagss_hdl_tr ) );
	
	//
	// Start the GSS session as a server.
	return 0;
}

void diagss_serv_fini(void)
{
	if (diagss_hdl_fb) {
		(void) fd_disp_unregister(&diagss_hdl_fb, NULL);
	}
	if (diagss_hdl_tr) {
		(void) fd_disp_unregister(&diagss_hdl_tr, NULL);
	}

	//
	// Cleanup & Closedown

	CHECK_FCT_DO( fd_sess_handler_destroy(&sess_handler, NULL), /* continue */ );

	return;
}
