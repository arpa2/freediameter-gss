/* diagss_app.c -- freeDiameter Extension app for Diameter-GSS
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Sebastien Decugis <sdecugis@freediameter.net>
 */

 
/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2013, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

/*
 * diagss application for freeDiameter.
 */

#include "diagss_server_app.h"

/* Initialize the configuration */
struct diagss_conf *diagss_conf = NULL;
static struct diagss_conf _conf;

static int diagss_conf_init(void)
{
	diagss_conf = &_conf;
	memset(diagss_conf, 0, sizeof(struct diagss_conf));

	/* Set the default values */
	diagss_conf->vendor_id  = 44469;    /* ARPA2 */
	diagss_conf->appli_id   = 1;        /* Network Access */
	diagss_conf->cmd_id     = 265;      /* AAR/AAA */
	diagss_conf->keytab = strdup("/etc/krb5.keytab");
	return 0;
}

static void diagss_conf_dump(void)
{
	if (!TRACE_BOOL(INFO))
		return;
	fd_log_debug( "------- diagss configuration dump: ---------");
	fd_log_debug( " Vendor Id .......... : %u", diagss_conf->vendor_id);
	fd_log_debug( " Application Id ..... : %u", diagss_conf->appli_id);
	fd_log_debug( " Command Id ......... : %u", diagss_conf->cmd_id);
	fd_log_debug( " Keytab ............. : %s", diagss_conf->keytab);
	fd_log_debug( "------- /diagss configuration dump ---------");
}

static void diagss_hook_cb_silent(enum fd_hook_type type, struct msg * msg, struct peer_hdr * peer, void * other, struct fd_hook_permsgdata *pmd, void * regdata) {
}
static void diagss_hook_cb_oneline(enum fd_hook_type type, struct msg * msg, struct peer_hdr * peer, void * other, struct fd_hook_permsgdata *pmd, void * regdata) {
	char * buf = NULL;
	size_t len;

	CHECK_MALLOC_DO( fd_msg_dump_summary(&buf, &len, NULL, msg, NULL, 0, 0),
		{ LOG_E("Error while dumping a message"); return; } );

	LOG_N("{%d} %s: %s", type, (char *)other ?:"<nil>", buf ?:"<nil>");

	free(buf);
}


/* entry point */
static int diagss_entry(char * conffile)
{
	TRACE_ENTRY("%p", conffile);

	/* Initialize configuration */
	CHECK_FCT( diagss_conf_init() );

	/* Parse configuration file */
	if (conffile != NULL) {
		CHECK_FCT( diagss_conf_handle(conffile) );
	}

	TRACE_DEBUG(INFO, "Extension diagss initialized with configuration: '%s'", conffile);
	diagss_conf_dump();

	/* Install objects definitions for this diagss application */
	CHECK_FCT( diagss_dict_init() );

	/* Install the handlers for incoming messages */
	CHECK_FCT( diagss_serv_init() );

	/* Advertise the support for the diagss application in the peer */
	CHECK_FCT( fd_disp_app_support ( diagss_appli, diagss_vendor, 1, 0 ) );

	return 0;
}

/* Unload */
void fd_ext_fini(void)
{
	diagss_serv_fini();
}

EXTENSION_ENTRY("diagss_server_app", diagss_entry);
