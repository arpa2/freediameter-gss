#ifndef DIAGSS_LIB_H
#define DIAGSS_LIB_H

#include <stdint.h>
#include <sys/types.h>

int get_spn_from_spnego(
	uint8_t *message, ssize_t message_size,
	char **realm, int *realm_len,
	char **service, int *service_len,
	char **host, int *host_len
);

#endif /* DIAGSS_LIB_H */
