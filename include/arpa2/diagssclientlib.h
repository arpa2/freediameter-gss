#ifndef DIAGSSCLIENT_LIB_H
#define DIAGSSCLIENT_LIB_H

#include <freeDiameter/freeDiameter-host.h>
#include <freeDiameter/libfdcore.h>

#include <signal.h>
#include <getopt.h>
#include <locale.h>
#include <syslog.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <unbound.h>

#include <arpa2/socket.h>

#include <gssapi/gssapi_generic.h>
#include <gssapi/gssapi_krb5.h>
#include <gssapi/gssapi_ext.h>

/* Some global variables for dictionary */
extern struct dict_object * diagss_vendor;
extern struct dict_object * diagss_appli;
extern struct dict_object * diagss_cmd_r;
extern struct dict_object * diagss_cmd_a;

extern struct dict_object * diagss_sess_id;
extern struct dict_object * diagss_origin_host;
extern struct dict_object * diagss_origin_realm;
extern struct dict_object * diagss_dest_host;
extern struct dict_object * diagss_dest_realm;
extern struct dict_object * diagss_user_name;
extern struct dict_object * diagss_res_code;
extern struct dict_object * diagss_avp_gss_token;

struct req_resp_t {
	pthread_mutex_t *mutex;
	int32_t		    rescode;
	int             token_length;
	void            *token_value;
	char            user_name[1024];
};

int client_establish_context(char *dest_realm, OM_uint32 *major_status);
void sync_req_resp_token(char *dest_realm, size_t dest_realm_len, void* token, size_t token_len, struct req_resp_t *req_resp);

/* Initialize dictionary definitions */
int diagss_dict_init(void);

/* lookup unbound server callback */
#define HOSTNAME_BUFSIZE 256

typedef bool (*iter_callback_t)(struct sockaddr_storage *ss, socklen_t salen, char* hostname, int port, void *user);
bool iter_srv(struct ub_ctx *ubctx, char *srvname, iter_callback_t iter_callback, void *user);

#endif /* DIAGSSCLIENT_LIB_H */