#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <unbound.h>

#include <arpa2/except.h>
#include <arpa2/diagssclientlib.h>

#define CL_IN     1
#define RR_SRV   33
#define RR_A      1
#define RR_AAAA  28

bool _dns2hostport (char *data, int datalen, char hostname [HOSTNAME_BUFSIZE], uint16_t *netport) {
	int hostofs = 0;
	char dataofs = 6;
	//
	// Harvest the port number as-is, that is, in network byte order
	if (datalen < 10) {
		/* Sillily small */
		return false;
	}
	* netport = * (uint16_t *) &data [4];
	//
	// Collect the labels of the host name
	while (data [dataofs] != 0) {
		unsigned int eltlen = data [dataofs];
		if (dataofs + eltlen + 1 > datalen) {
			return false;
		}
		if (hostofs + eltlen + 1 > HOSTNAME_BUFSIZE) {
			return false;
		}
		memcpy (hostname + hostofs, data + dataofs + 1, eltlen);
		hostname [hostofs + eltlen] = '.';
		hostofs += eltlen + 1;
		dataofs += eltlen + 1;
	}
	//
	// We should not return an empty string
	if (hostofs == 0) {
		return false;
	} else {
		hostname [hostofs-1] = 0;
		return true;
	}
}

#define UB_CHECK_RESULT(result) (!result->havedata || result->bogus || result->nxdomain)

bool iter_ipv4(struct ub_result *a, char* hostname, int port, iter_callback_t iter_callback, void *user)
{
	struct sockaddr_storage ss;
	memset(&ss, 0, sizeof(ss));
	bool rc = false;
	assert (a != NULL);
	if (UB_CHECK_RESULT(a)) {
		log_errno("RR A: ub result check fails");
	} else {
		for (int i = 0; !rc && a->data[i]; i++) {
			/* Copy IPv4 address and port */
			((struct sockaddr *) &ss)->sa_family = AF_INET;
			memcpy (&((struct sockaddr_in *) &ss)->sin_addr, a->data[i], 4);
			((struct sockaddr_in *) &ss)->sin_port = port;

			rc = iter_callback(&ss, sizeof (struct sockaddr_in), hostname, ntohs(port), user);
		}
	}
	return rc;
}

bool iter_ipv6(struct ub_result *aaaa, char* hostname, int port, iter_callback_t iter_callback, void *user)
{
	struct sockaddr_storage ss;
	memset(&ss, 0, sizeof(ss));

	bool rc = false;
	assert (aaaa != NULL);
	if (UB_CHECK_RESULT(aaaa)) {
		log_errno("RR AAAA: ub result check fails");
	} else {
		for (int i = 0; !rc && aaaa->data[i]; i++) {
			/* Copy IPv6 address and port */
			((struct sockaddr *) &ss)->sa_family = AF_INET6;
			memcpy (&((struct sockaddr_in6 *) &ss)->sin6_addr, aaaa->data[i], 16);
			((struct sockaddr_in6 *) &ss)->sin6_port = port;
			rc = iter_callback(&ss, sizeof (struct sockaddr_in6), hostname, ntohs(port), user);
		}
	}
	return rc;
}

bool iter_srv(struct ub_ctx *ubctx, char *srvname, iter_callback_t iter_callback, void *user)
{
	bool rc = false;
	//
	// Lookup the SRV record to find one or more host/port pairs
	struct ub_result *srv;
	log_debug ("Resolving IN SRV for %s", srvname);
	if (ub_resolve (ubctx, srvname, RR_SRV, CL_IN, &srv) != 0) {
		log_errno ("ub_resolve");
	} else {
		assert (srv != NULL);
		if (UB_CHECK_RESULT(srv)) {
			log_errno ("RR SRV: ub result check fails");
		} else {
			for (int iter_srv = 0; srv->data[iter_srv]; iter_srv++) {
				uint16_t netport;
				char hostname [HOSTNAME_BUFSIZE];
				if (_dns2hostport (srv->data [iter_srv], srv->len [iter_srv], hostname, &netport)) {
					log_debug("Following SRV #%d to %s:%d", iter_srv, hostname, ntohs (netport));
					//
					// Find the IP addresses for the host name in the SRV record
					struct ub_result *aaaa;
					log_debug("Resolving IN AAAA for %s", hostname);
					if (ub_resolve (ubctx, hostname, RR_AAAA, CL_IN, &aaaa) != 0) {
						/* This should never fail */
						log_errno("ub_resolve != 0");
					} else {
						rc = iter_ipv6(aaaa, hostname, netport, iter_callback, user);
						ub_resolve_free (aaaa);
						if (rc) {
							break;
						}
					}
					struct ub_result *a;
					log_debug("Resolving IN A for %s", hostname);
					if (ub_resolve (ubctx, hostname, RR_A, CL_IN, &a) != 0) {
						/* This should never fail */
						log_errno("ub_resolve != 0");
					} else {
						rc = iter_ipv4(a, hostname, netport, iter_callback, user);
						ub_resolve_free (a);
						if (rc) {
							break;
						}
					}
				}
			}
		}
		ub_resolve_free(srv);
	}
	return rc;
}

