/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2020, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

#if defined(__GLIBC__)
#define _BSD_SOURCE /* for vsyslog */
#endif

#include <ctype.h>
#include <arpa2/diagsslib.h>
#include <arpa2/diagssclientlib.h>

static gss_name_t target_name;

static int verbose = 1;
static int spnego = 1;
static gss_OID_desc gss_spnego_mechanism_oid_desc =
{6, (void *)"\x2b\x06\x01\x05\x05\x02"};
static gss_ctx_id_t context = GSS_C_NO_CONTEXT;

static void req_resp_token(char *dest_realm, size_t dest_realm_len, void* token, size_t token_len, struct req_resp_t *req_resp);

void display_status(char * prompt, OM_uint32 maj_stat, OM_uint32 min_stat)
{
	fd_log_error("Error: %s, maj_stat %d, min_stat %d", prompt, maj_stat, min_stat);
}

static gss_OID_set_desc *get_mechs(gss_OID oid)
{
	static gss_OID_set_desc mechs;

	gss_OID_set_desc *mechsp = GSS_C_NO_OID_SET;
	if (spnego) {
		mechs.elements = &gss_spnego_mechanism_oid_desc;
		mechs.count = 1;
		mechsp = &mechs;
	} else if (oid != GSS_C_NO_OID) {
		mechs.elements = oid;
		mechs.count = 1;
		mechsp = &mechs;
	} else {
		mechs.elements = NULL;
		mechs.count = 0;
	}
	return mechsp;
}

static int client_get_credential(
	OM_uint32 gss_flags,
	gss_OID oid,
	gss_OID_set_desc *mechsp,
	char *username,
	char *password,
	gss_cred_id_t *cred
)
{
	gss_buffer_desc send_tok;
	OM_uint32 maj_stat, min_stat;
	*cred = GSS_C_NO_CREDENTIAL;
	gss_name_t gss_username = GSS_C_NO_NAME;

	if (username != NULL) {
		send_tok.value = username;
		send_tok.length = strlen(username);

		maj_stat = gss_import_name(&min_stat, &send_tok,
								   (gss_OID) gss_nt_user_name,
								   &gss_username);
		if (maj_stat != GSS_S_COMPLETE) {
			display_status("parsing client name", maj_stat, min_stat);
			return -1;
		}
	}

	if (password != NULL) {
		gss_buffer_desc pwbuf;

		pwbuf.value = password;
		pwbuf.length = strlen(password);

		maj_stat = gss_acquire_cred_with_password(&min_stat,
												  gss_username,
												  &pwbuf, 0,
												  mechsp, GSS_C_INITIATE,
												  cred, NULL, NULL);
	} else if (gss_username != GSS_C_NO_NAME) {
		maj_stat = gss_acquire_cred(&min_stat,
									gss_username, 0,
									mechsp, GSS_C_INITIATE,
									cred, NULL, NULL);
	} else
		maj_stat = GSS_S_COMPLETE;
	if (maj_stat != GSS_S_COMPLETE) {
		display_status("acquiring creds", maj_stat, min_stat);
		gss_release_name(&min_stat, &gss_username);
		return -1;
	}
	if (spnego && oid != GSS_C_NO_OID) {
		gss_OID_set_desc neg_mechs;

		neg_mechs.elements = oid;
		neg_mechs.count = 1;

		maj_stat = gss_set_neg_mechs(&min_stat, *cred, &neg_mechs);
		if (maj_stat != GSS_S_COMPLETE) {
			display_status("setting neg mechs", maj_stat, min_stat);
			gss_release_name(&min_stat, &gss_username);
			gss_release_cred(&min_stat, cred);
			return -1;
		}
	}
	gss_release_name(&min_stat, &gss_username);

	return 0;
}

static int client_context_step(
	char *dest_realm,
	gss_OID_set_desc *mechsp,
	gss_cred_id_t *cred,
	OM_uint32 gss_flags,
	gss_OID oid,
	gss_ctx_id_t *gss_context,
	gss_buffer_desc *token_ptr,
	OM_uint32 *ret_flags,
	struct req_resp_t *req_resp,
	OM_uint32 *major_status
)
{
	OM_uint32 maj_stat, min_stat, init_sec_min_stat;
    gss_buffer_desc send_tok;

	maj_stat = gss_init_sec_context(
		&init_sec_min_stat,
		*cred, gss_context,
		target_name, mechsp->elements,
		gss_flags, 0,
		NULL, /* channel bindings */
		token_ptr, NULL, /* mech type */
		&send_tok, ret_flags,
		NULL /* time_rec */
	);  

	if (token_ptr != GSS_C_NO_BUFFER)
		free(req_resp->token_value);

	if (send_tok.length != 0) {
		if (verbose)
			printf("Sending init_sec_context token with size = %d\n",
				   (int) send_tok.length);
		char *realm;
		int realm_len;
		char *service;
		int service_len;
		char *host;
		int host_len;

		int r = get_spn_from_spnego(
			send_tok.value, send_tok.length,
			&realm, &realm_len,
			&service, &service_len,
			&host, &host_len
		);
		
		if (r == 0) {
			char *realm_lower = malloc(realm_len);
			if (realm_lower) {
				for (int i = 0; i < realm_len; i++) {
					realm_lower[i] = tolower(realm[i]);
				}
				printf("realm: %.*s\n", realm_len, realm);
				printf("realm_lower: %.*s\n", realm_len, realm_lower);
				printf("service: %.*s\n", service_len, service);
				printf("host: %.*s\n", host_len, host);
				gss_buffer_desc name_buf;
				name_buf.value = service;
				name_buf.length = service_len;
				sync_req_resp_token(realm_lower, realm_len, send_tok.value, send_tok.length, req_resp);
				free(realm_lower);
			}
			
		}
	}
	(void) gss_release_buffer(&min_stat, &send_tok);
	*major_status = maj_stat;
	return 0;
}

/* Callback called when a GSS token is received */
static void gss_cb_ans(void * data, struct msg ** msg)
{
	struct avp * avp;
	struct avp_hdr * hdr;
	int error = 0;
	struct req_resp_t *req_resp = (struct req_resp_t *) data;

	/* Now log content of the answer */
	fprintf(stderr, "RECV ");
	
	/* Check the GSS_Token Payload AVP */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diagss_avp_gss_token, &avp), return );
	if (avp) {
		struct avp_hdr * hdr;

		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return  );
		req_resp->token_length = hdr->avp_value->os.len;
		req_resp->token_value = malloc(req_resp->token_length);
		memcpy(req_resp->token_value, hdr->avp_value->os.data, req_resp->token_length);
		fd_log_debug("Found GSS_Token Payload AVP");
	} else {
		fd_log_debug("GSS_Token Payload AVP not present");
	}
	
	/* Check the User-Name AVP */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diagss_user_name, &avp), return );
	if (avp) {
		struct avp_hdr * hdr;

		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return  );
		memcpy(req_resp->user_name, hdr->avp_value->os.data, hdr->avp_value->os.len);
		req_resp->user_name[hdr->avp_value->os.len] = '\0';
		fd_log_debug("Found User-Name AVP");
	} else {
		fd_log_debug("User-Name AVP *not* found");
	}

	/* Value of Result Code */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diagss_res_code, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		fprintf(stderr, "Status: %d ", hdr->avp_value->i32);
		req_resp->rescode = hdr->avp_value->i32;
		if (hdr->avp_value->i32 != 2001)
			error++;
	} else {
		fprintf(stderr, "no_Result-Code ");
		error++;
	}
	
	/* Value of Origin-Host */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diagss_origin_host, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		fprintf(stderr, "From '%.*s' ", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
	} else {
		fprintf(stderr, "no_Origin-Host ");
		error++;
	}
	
	/* Value of Origin-Realm */
	CHECK_FCT_DO( fd_msg_search_avp ( *msg, diagss_origin_realm, &avp), return );
	if (avp) {
		CHECK_FCT_DO( fd_msg_avp_hdr( avp, &hdr ), return );
		fprintf(stderr, "('%.*s') ", (int)hdr->avp_value->os.len, hdr->avp_value->os.data);
	} else {
		fprintf(stderr, "no_Origin-Realm ");
		error++;
	}
	
	
	fflush(stderr);
	
	/* Free the message */
	CHECK_FCT_DO(fd_msg_free(*msg), return);
	*msg = NULL;
	
	pthread_mutex_unlock(req_resp->mutex);
	return;
}

/* Send and receive a GSS token */
static void req_resp_token(char *dest_realm, size_t dest_realm_len, void* token, size_t token_len, struct req_resp_t *req_resp)
{
	struct msg * req = NULL;
	struct avp * avp;
	union avp_value val;
	struct sess_state * mi = NULL, *svg;
	struct session *sess = NULL;
	
	TRACE_DEBUG(FULL, "Creating a new message for sending.");
	
	/* Create the request */
	CHECK_FCT_DO( fd_msg_new( diagss_cmd_r, MSGFL_ALLOC_ETEID, &req ), goto out );
		
	/* Now set all AVPs values */
	
	/* Set the Destination-Realm AVP */
	{
		CHECK_FCT_DO( fd_msg_avp_new ( diagss_dest_realm, 0, &avp ), goto out  );
		val.os.data = (unsigned char *) dest_realm;
		val.os.len  = dest_realm_len;
		CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
		CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
	}
	
	/* Set Origin-Host & Origin-Realm */
	CHECK_FCT_DO( fd_msg_add_origin ( req, 0 ), goto out  );
	
	/* Set the GSS-Token AVP */
	CHECK_FCT_DO( fd_msg_avp_new ( diagss_avp_gss_token, 0, &avp ), goto out  );
	val.os.data = (unsigned char *) token;
	val.os.len  = token_len;
	CHECK_FCT_DO( fd_msg_avp_setvalue( avp, &val ), goto out  );
	CHECK_FCT_DO( fd_msg_avp_add( req, MSG_BRW_LAST_CHILD, avp ), goto out  );
		
	/* Log sending the message */
	fprintf(stderr, "SEND to '%s'\n", dest_realm);
	fflush(stderr);
	
	/* Send the request */
	CHECK_FCT_DO( fd_msg_send( &req, gss_cb_ans, req_resp ), goto out );

out:
	return;
}

void sync_req_resp_token(char *dest_realm, size_t dest_realm_len, void* token, size_t token_len, struct req_resp_t *req_resp)
{
	pthread_mutex_t mutex;
	pthread_mutex_init(&mutex, NULL);
	pthread_mutex_lock(&mutex);
	req_resp->mutex = &mutex;
	fprintf(stderr, "Calling req_resp_token, waiting for answer, dest_realm: %.*s\n", dest_realm_len, dest_realm);
	req_resp_token(dest_realm, dest_realm_len, token, token_len, req_resp);
	// wait for answer
	pthread_mutex_lock(&mutex);
	fprintf(stderr, "Got answer, rescode %d, user_name: %s\n", req_resp->rescode, req_resp->user_name);
	pthread_mutex_destroy(&mutex);

}

int client_establish_context(char *dest_realm, OM_uint32 *major_status)
{
	/*
	 * Import the name into target_name.
	 */
	char *service_name = "diameters";
    gss_buffer_desc service_tok;
	OM_uint32 maj_stat, min_stat;

	service_tok.value = service_name;
	service_tok.length = strlen(service_name);
	maj_stat = gss_import_name(
		&min_stat, &service_tok,
		(gss_OID) gss_nt_service_name,
		&target_name
	);
	if (maj_stat != GSS_S_COMPLETE) {
		display_status("parsing name", maj_stat, min_stat);
		return -1;
	}
	/*
	 * Get mechanisms
	 */
	OM_uint32 gss_flags = GSS_C_MUTUAL_FLAG | GSS_C_REPLAY_FLAG;
	gss_OID oid = GSS_C_NULL_OID;
    OM_uint32 ret_flags;
	gss_cred_id_t cred;
	gss_OID_set_desc *mechsp;

	mechsp = get_mechs(oid);
    if (client_get_credential(gss_flags, oid, mechsp, NULL, NULL, &cred) < 0) {
		return -1;
	}
	gss_ctx_id_t gss_context = GSS_C_NO_CONTEXT;
	gss_buffer_desc *token_ptr = GSS_C_NO_BUFFER;
	struct req_resp_t req_resp;
	req_resp.token_value = NULL;
	req_resp.token_length = 0;
	gss_buffer_desc gss_token;

	do
	{
		if (client_context_step(
			dest_realm,
			mechsp,
			&cred,
			gss_flags,
			oid,
			&gss_context,
			token_ptr,
			&ret_flags,
			&req_resp,
			&maj_stat
		) < 0) {
			return -1;
		}
		gss_token.value = req_resp.token_value;
		gss_token.length = req_resp.token_length;
		token_ptr = &gss_token;
		fprintf(stderr, "maj_stat: %x\n", maj_stat);
	} while (maj_stat == GSS_S_CONTINUE_NEEDED);
	*major_status = maj_stat;
	return 0;
}
