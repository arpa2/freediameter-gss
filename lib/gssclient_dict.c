/* diagss_dict.c -- freeDiameter Extension app/dictionary for Diameter-GSS
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Sebastien Decugis <sdecugis@freediameter.net>
 */

/*********************************************************************************************************
* Software License Agreement (BSD License)                                                               *
* Author: Sebastien Decugis <sdecugis@freediameter.net>							 *
*													 *
* Copyright (c) 2013, WIDE Project and NICT								 *
* All rights reserved.											 *
* 													 *
* Redistribution and use of this software in source and binary forms, with or without modification, are  *
* permitted provided that the following conditions are met:						 *
* 													 *
* * Redistributions of source code must retain the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer.										 *
*    													 *
* * Redistributions in binary form must reproduce the above 						 *
*   copyright notice, this list of conditions and the 							 *
*   following disclaimer in the documentation and/or other						 *
*   materials provided with the distribution.								 *
* 													 *
* * Neither the name of the WIDE Project or NICT nor the 						 *
*   names of its contributors may be used to endorse or 						 *
*   promote products derived from this software without 						 *
*   specific prior written permission of WIDE Project and 						 *
*   NICT.												 *
* 													 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED *
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A *
* PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 	 *
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 	 *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR *
* TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF   *
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.								 *
*********************************************************************************************************/

/* Install the dictionary objects */

#include <freeDiameter/extension.h>

#include <arpa2/gssclient_dict.h>

struct dict_object * diagss_vendor = NULL;
struct dict_object * diagss_appli = NULL;
struct dict_object * diagss_cmd_r = NULL;
struct dict_object * diagss_cmd_a = NULL;

struct dict_object * diagss_sess_id = NULL; 
struct dict_object * diagss_origin_host = NULL;
struct dict_object * diagss_origin_realm = NULL;
struct dict_object * diagss_dest_host = NULL;
struct dict_object * diagss_dest_realm = NULL;
struct dict_object * diagss_user_name = NULL;
struct dict_object * diagss_res_code = NULL;
struct dict_object * diagss_avp_gss_token = NULL;

int diagss_dict_init(void)
{
	TRACE_DEBUG(FULL, "Initializing the dictionary for AAR/AAA");
	
	/* Create the diagss Vendor (ARPA2) */
	{
		struct dict_vendor_data data;
		data.vendor_id = diagss_conf->vendor_id;
		data.vendor_name = "diagss vendor";
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_VENDOR, &data, NULL, &diagss_vendor));
	}
	
	/* Create the diagss Application */
	{
		struct dict_application_data data;
		data.application_id = diagss_conf->appli_id;
		data.application_name = "diagss application";
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_APPLICATION, &data, diagss_vendor, &diagss_appli));
	}
	
	/* Create the AAR & AAA commands */
	{
		struct dict_cmd_data data;
		data.cmd_code = diagss_conf->cmd_id;
		data.cmd_name = "AAR";
		data.cmd_flag_mask = CMD_FLAG_PROXIABLE | CMD_FLAG_REQUEST;
		data.cmd_flag_val  = CMD_FLAG_PROXIABLE | CMD_FLAG_REQUEST;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_COMMAND, &data, diagss_appli, &diagss_cmd_r));
		data.cmd_name = "AAA";
		data.cmd_flag_val  = CMD_FLAG_PROXIABLE;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_COMMAND, &data, diagss_appli, &diagss_cmd_a));
	}
	
	/* Create the GSS-Token Payload AVP */
	{
		struct dict_avp_data data;
		data.avp_code = 16660011;
		data.avp_vendor = diagss_conf->vendor_id;
		data.avp_name = "GSS-Token";
		data.avp_flag_mask = AVP_FLAG_VENDOR;
		data.avp_flag_val = AVP_FLAG_VENDOR;
		data.avp_basetype = AVP_TYPE_OCTETSTRING;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_AVP, &data, NULL, &diagss_avp_gss_token));
	}
	
	/* Now resolve some other useful AVPs */
	CHECK_FCT( fd_dict_search( fd_g_config->cnf_dict, DICT_AVP, AVP_BY_NAME, "Session-Id", &diagss_sess_id, ENOENT) ); 
	CHECK_FCT( fd_dict_search( fd_g_config->cnf_dict, DICT_AVP, AVP_BY_NAME, "Origin-Host", &diagss_origin_host, ENOENT) );
	CHECK_FCT( fd_dict_search( fd_g_config->cnf_dict, DICT_AVP, AVP_BY_NAME, "Origin-Realm", &diagss_origin_realm, ENOENT) );
	CHECK_FCT( fd_dict_search( fd_g_config->cnf_dict, DICT_AVP, AVP_BY_NAME, "Destination-Host", &diagss_dest_host, ENOENT) );
	CHECK_FCT( fd_dict_search( fd_g_config->cnf_dict, DICT_AVP, AVP_BY_NAME, "Destination-Realm", &diagss_dest_realm, ENOENT) );
	CHECK_FCT( fd_dict_search( fd_g_config->cnf_dict, DICT_AVP, AVP_BY_NAME, "User-Name", &diagss_user_name, ENOENT) );
	CHECK_FCT( fd_dict_search( fd_g_config->cnf_dict, DICT_AVP, AVP_BY_NAME, "Result-Code", &diagss_res_code, ENOENT) );
	
	/* Create the rules for AAR and AAA */
	{
		struct dict_rule_data data;
		data.rule_max = 1;
		
		/* Session-Id is in first position */
		data.rule_min = 0;
		data.rule_avp = diagss_sess_id;
		data.rule_position = RULE_OPTIONAL;
		data.rule_order = 1;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_r, NULL));
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_a, NULL)); 

		data.rule_min = 1;
		/* Origin Host and Realm are mandatory */
		data.rule_avp = diagss_origin_host;
		data.rule_position = RULE_REQUIRED;

		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_r, NULL));
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_a, NULL));
		
		data.rule_avp = diagss_origin_realm;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_r, NULL));
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_a, NULL));
		
		/* And Result-Code is mandatory for answers only */
		data.rule_avp = diagss_res_code;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_a, NULL));
		
		/* And Destination-Realm for requests only */
		data.rule_avp = diagss_dest_realm;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_r, NULL));
		
		/* And Destination-Host optional for requests only */
		data.rule_position = RULE_OPTIONAL;
		data.rule_min = 0;
		data.rule_avp = diagss_dest_host;
		CHECK_FCT(fd_dict_new( fd_g_config->cnf_dict, DICT_RULE, &data, diagss_cmd_r, NULL));
		
	}
	
	return 0;
}
